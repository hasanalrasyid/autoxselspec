# autoXSelSpec

This is a project to Automate the XSpec and XSelect program as part of HEASOFT package. This program would automate data processing on a Gamma Ray Burst (GRB) data.

For the moment, this repository would hold the latest compiled binary of our code. Our intention is to publish the source code after we have a more stable version in no less than by the end of this year. Thank you for your understanding.

## Getting started

This binary was compiled in Springdale Linux 8 environment.
There are several program requirements that need to be meet before you run this code:

- HEASOFT package (v6.28), with XSPEC (v12.11.1)
- ds9 package
- expect
- python3

More requirements will be reflected on the updates of this file.

To start, just clone this repository (or download the binary Main.dev) to your path, and invoke the program as an executable.

```
git clone https://gitlab.com/hasanalrasyid/autoxselspec.git
cd autoxselspec
./Main.dev
```

## Collaboration

For the moment, we accept the bug reports and very limited set of feature request.
Our current priority is to make a complete binary for Photon Counting mode (PC) and Window Timing mode (WT) run as our 0.1.0 release target.
After this release (in sync with the source code), we would honoured to accept any merge request from the community. Please be patient and stay tuned.

Please go to [The Issue Tracker](https://gitlab.com/hasanalrasyid/autoxselspec/-/issues) to create the issues related with this project.

## Usage

Run the program with:

~~~
Main.dev [options]
~~~

## License

autoXSelSpec is free software released under the [GNU Affero General Public License](LICENSE.txt)
